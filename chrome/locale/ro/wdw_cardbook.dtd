<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Activează sau dezactivează căutarea şi sincronizarea agendelor.">
<!ENTITY colorAccountsTooltip "Colorează rezultatele căutării în agende (atunci când căutările se fac în mai multe agende)">
<!ENTITY readonlyAccountsTooltip "Setează agendele în mod „doar citire” sau „citire-scriere”.">

<!ENTITY cardbookAccountMenuLabel "Agendă">

<!ENTITY cardbookAccountMenuAddServerLabel "Agendă nouă">
<!ENTITY cardbookAccountMenuEditServerLabel "Editează agenda">
<!ENTITY cardbookAccountMenuCloseServerLabel "Șterge agenda">
<!ENTITY cardbookAccountMenuSyncLabel "Sincronizează agenda">
<!ENTITY cardbookAccountMenuSyncsLabel "Sincronizează toate agendele">

<!ENTITY cardbookContactsMenuLabel "Contacte">

<!ENTITY cardbookToolsMenuLabel "Unelte">
<!ENTITY cardbookToolsMenuBirthdayListLabel "Vezi lista aniversărilor">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Adăugă aniversări în calendar">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Găsește contacte duplicat în toate agendele">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Găsește contacte duplicat în agenda curentă">
<!ENTITY cardbookToolsMenuPrefsLabel "Preferinţe CardBook">

<!ENTITY cardbookToolbarLabel "Bară de unelte pentru CardBook">
<!ENTITY cardbookToolbarAccesskey "a">
<!ENTITY cardbookABPaneToolbarLabel "Bară de unelte pentru agendă CardBook">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "Personalizare…">
<!ENTITY CustomizeCardBookToolbarAccesskey "P">
<!ENTITY cardbookToolbarAddServerButtonLabel "Agendă nouă">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Adaugă o agendă la distanță sau locală">
<!ENTITY cardbookToolbarSyncButtonLabel "Sincronizează">
<!ENTITY cardbookToolbarSyncButtonTooltip "Sincronizează toate agendele la distanță">
<!ENTITY cardbookToolbarWriteButtonLabel "Scrie">
<!ENTITY cardbookToolbarWriteButtonTooltip "Creează un mesaj nou">
<!ENTITY cardbookToolbarChatButtonLabel "Mesaj instant">
<!ENTITY cardbookToolbarChatButtonTooltip "Trimite un mesaj instant sau chat">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Preferinţe">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "Preferințe CardBook">
<!ENTITY cardbookToolbarAddContactButtonLabel "Contact nou">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Creează un contact nou în agendă">
<!ENTITY cardbookToolbarAddListButtonLabel "Listă nouă">
<!ENTITY cardbookToolbarAddListButtonTooltip "Creează o listă nouă cu adrese de email">
<!ENTITY cardbookToolbarEditButtonLabel "Modifică">
<!ENTITY cardbookToolbarEditButtonTooltip "Modifică contactul selectat">
<!ENTITY cardbookToolbarRemoveButtonLabel "Şterge">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Șterge contactul selectat">
<!ENTITY cardbookToolbarPrintButtonLabel "Previzualizează tipărirea">
<!ENTITY cardbookToolbarPrintButtonTooltip "Previzualizează tipărirea selecției">
<!ENTITY cardbookToolbarAppMenuButtonLabel "Meniu CardBook">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "Meniu CardBook">
<!ENTITY cardbookToolbarSearchBoxLabel "Caută contacte">
<!ENTITY cardbookToolbarSearchBoxTooltip "Caută contacte">
<!ENTITY cardbookToolbarComplexSearchLabel "Căutare salvată">
<!ENTITY cardbookToolbarComplexSearchTooltip "Creează un dosar nou de căutare salvată">
<!ENTITY cardbookToolbarThMenuButtonLabel "AppMenu">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Afişează meniu &brandShortName;">

<!ENTITY generalTabLabel "General">
<!ENTITY mailPopularityTabLabel "Popularitate email">
<!ENTITY technicalTabLabel "Tehnic">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Categorii">
<!ENTITY noteTabLabel "Notițe">
<!ENTITY advancedTabLabel "Avansat">

<!ENTITY miscGroupboxLabel "Diverse">
<!ENTITY othersGroupboxLabel "Alte">
<!ENTITY techGroupboxLabel "Tehnic">
<!ENTITY labelGroupboxLabel "Etichete">
<!ENTITY class1Label "Clasă">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Mailere">
<!ENTITY agentLabel "Agent">
<!ENTITY keyLabel "Cheie">
<!ENTITY photolocalURILabel "Poză locală">
<!ENTITY logolocalURILabel "Siglă locală">
<!ENTITY soundlocalURILabel "Sunet local">
<!ENTITY photoURILabel "Poză">
<!ENTITY logoURILabel "Siglă">
<!ENTITY soundURILabel "Sunet">
<!ENTITY prodidLabel "ID produs">
<!ENTITY sortstringLabel "Cuvănt sortare">
<!ENTITY uidLabel "ID Contact">
<!ENTITY versionLabel "Versiune">
<!ENTITY tzLabel "Fus orar">

<!ENTITY dirPrefIdLabel "ID Agendă">
<!ENTITY cardurlLabel "Adresă URL a cartelei">
<!ENTITY cacheuriLabel "Cache URI">
<!ENTITY revLabel "Ultima actualizare">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Arată pe hartă">
<!ENTITY toEmailEmailTreeLabel "Scrie un mesaj nou">
<!ENTITY ccEmailEmailTreeLabel "Scrie un mesaj nou (copie / cc)">
<!ENTITY bccemailemailTreeLabel "Scrie un mesaj nou (copie ascunsă / bcc)">
<!ENTITY findemailemailTreeLabel "Găseşte email-uri în legătură cu această adresă">
<!ENTITY findeventemailTreeLabel "Găsește evenimente din calendar în legătură cu această adresă">
<!ENTITY openURLTreeLabel "Deschide adresa URL">

<!ENTITY copyCardTreeLabel "Copiază înregistrarea">
<!ENTITY pasteCardTreeLabel "Lipeşte înregistrarea">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Scrie un mesaj nou">
<!ENTITY toEmailCardFromCardsLabel "Scrie un mesaj nou">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Scrie un mesaj nou (copie / cc)">
<!ENTITY ccEmailCardFromCardsLabel "Scrie un mesaj nou (copie / cc)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Scrie un mesaj nou (copie ascunsă / bcc)">
<!ENTITY bccEmailCardFromCardsLabel "Scrie un mesaj nou (copie ascunsă/ bcc)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Distribuie prin email">
<!ENTITY shareCardByEmailFromCardsLabel "Distribuie prin email">
<!ENTITY categoryLabel "Categorie">
<!ENTITY findEmailsFromCardsLabel "Găseşte email-uri în legătură cu acest contact">
<!ENTITY findEventsFromCardsLabel "Găsește evenimente în calendar în legătură cu acest contact">
<!ENTITY localizeCardFromCardsLabel "Arată pe hartă">
<!ENTITY openURLCardFromCardsLabel "Deschide adresele URL">
<!ENTITY cutCardFromAccountsOrCatsLabel "Taie">
<!ENTITY cutCardFromCardsLabel "Taie">
<!ENTITY copyCardFromAccountsOrCatsLabel "Copiază">
<!ENTITY copyCardFromCardsLabel "Copiază">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Lipeşte">
<!ENTITY pasteCardFromCardsLabel "Lipeşte">
<!ENTITY exportCardToFileLabel "Exportă agenda într-un fișier">
<!ENTITY exportCardToDirLabel "Exportă agenda într-un director">
<!ENTITY importCardFromFileLabel "Importă contacte dintr-un fișier">
<!ENTITY importCardFromDirLabel "Importă contacte dintr-un director">
<!ENTITY renameCatFromAccountsOrCatsLabel "Redenumește categoria">
<!ENTITY convertCatFromAccountsOrCatsLabel "Convertește categoria în listă">
<!ENTITY removeCatFromAccountsOrCatsLabel "Elimină categoria">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Găsește contacte duplicat în agenda curentă">
<!ENTITY generateFnFromAccountsOrCatsLabel "Generează nume afișate">
<!ENTITY mergeCardsFromCardsLabel "Îmbină contactele">
<!ENTITY duplicateCardFromCardsLabel "Dublează contactul">
<!ENTITY convertListToCategoryFromCardsLabel "Convertește lista în categorie">
<!ENTITY editAccountFromAccountsOrCatsLabel "Editează agenda">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Sincronizează agenda">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Șterge agenda">

<!ENTITY IMPPMenuLabel "Mesaj instant">
